function Level(map1, map2, _game, _stage, _audio, _fx) {

    var map1 = map1;
    var map2 = map2;

    this.audio = _audio;

    var tileW = 64, tileH = 64;

    // context of both maps
    var ctxMap1 = map1.canvas.getContext("2d");
    var ctxMap2 = map2.canvas.getContext("2d");

    // representation of the current map 
    var currentMap = null;
    var imagesMap = null;
    
    var levelId = null;
    this.getCurrentLevel = function() {
        return levelId;   
    };
    
    // assets loading
    var wall_borderless, wall_borderless_2, wall_borderless_3, wall_borderless_4, wall_borderless_5, wall_borderless_6,
        wall_borderless_7, wall_borderless_8, wall_borderless_9, wall_bordered, wall_bordertop, wall_borderbottom,
        road, road2, road3, road4, road5, road6, road7, road8, road9;
    this.loader = null;
    this.loaded = false;
    this.loadedAssets = function(loader) {
        wall_borderless = loader.getResult("./img/champ_borderless.png");
        wall_borderless_2 = loader.getResult("./img/champ_borderless_2.png");
        wall_borderless_3 = loader.getResult("./img/champ_borderless_3.png");
        wall_borderless_4 = loader.getResult("./img/champ_borderless_4.png");
        wall_borderless_5 = loader.getResult("./img/champ_borderless_5.png");
        wall_borderless_6 = loader.getResult("./img/champ_borderless_6.png");
        wall_borderless_7 = loader.getResult("./img/champ_borderless_7.png");
        wall_borderless_8 = loader.getResult("./img/champ_borderless_8.png");
        wall_borderless_9 = loader.getResult("./img/champ_borderless_9.png");
        wall_bordered = loader.getResult("./img/champ.png");
        wall_bordertop = loader.getResult("./img/champ_borderless_bas.png");
        wall_borderbottom = loader.getResult("./img/champ_borderless_haut.png");
        road = loader.getResult("./img/terre.png");
        road2 = loader.getResult("./img/terre.png");
        road3 = loader.getResult("./img/terre_3.png");
        road4 = loader.getResult("./img/terre_4.png");
        road5 = loader.getResult("./img/terre_5.png");
        road6 = loader.getResult("./img/terre_6.png");
        road7 = loader.getResult("./img/terre_7.png");
        road8 = loader.getResult("./img/terre_8.png");
        road9 = loader.getResult("./img/terre_9.png");
        this.loader = loader;
        this.loaded = true;
    }
    

    /**
     *  Personnages, ennemis, potions
     *  (instanciés par la fonction load)
     */
    this.player1 = null;
    this.player2 = null;
    this.enemies = [];
    this.pickups = [];


    /** 
     *  Chargement d'un niveau et instanciation des éléments le constituant.
     *  Hypothèse : la carte est préchargée dans le loader
     */
    this.load = function(id, callback) {
        if (!this.loaded) {
            console.error("Loader has not been defined.");
            return;
        }
        // chargement image du décor dans un canvas spécifique
        var pixelmap = document.createElement("canvas");
        var ctx = pixelmap.getContext("2d");
        var that = this;
        var image = this.loader.getResult("./maps/map" + id + ".png");
        if (!image) {
            console.error("Map ", id, "does not exist.");
            return;
        }
        levelId = id;
        
        // load pixel buffer
        pixelmap.width = image.width;
        pixelmap.height = image.height;
        ctx.width = image.width;
        ctx.height = image.height;
        ctx.drawImage(image, 0, 0);
        console.log("map size :", ctx.width, ctx.height);
        
        // transform current map into readable format
        currentMap = [];
        that.enemies = [];
        that.pickups = [];
        
        // reading map elements
        // change w.r.t. before --> 
        var pickups = [];
        for (var j=0; j < ctx.height; j++) {
            currentMap[j] = [];
            for (var i=0; i < ctx.width; i++) {
                var pixel = ctx.getImageData(i, j, 1, 1).data;
                currentMap[j][i] = 0;
                var hexa = pixel[0].toString(16) + pixel[1].toString(16) + pixel[2].toString(16);
                // console.log(hexa);
                switch (hexa) {
                    case "000":
                        currentMap[j][i] = 1;
                        break;
                    case "ff5555":
                        // console.log("Enemy at ", i*tileW, j*tileH);
                        that.enemies.push(new Enemy((i+0.5)*tileW, (j+0.5)*tileH, "tutu", _stage, that));
                        break;
                    case "00ff":
                        // console.log("Enemy at ", i*tileW, j*tileH);
                        that.enemies.push(new Enemy((i+0.5)*tileW, (j+0.5)*tileH, "pomp", _stage, that));
                        break;
                    case "0aa0":
                        // console.log("Enemy at ", i*tileW, j*tileH);
                        that.enemies.push(new Enemy((i+0.5)*tileW, (j+0.5)*tileH, "metal", _stage, that));
                        break;
                    case "ffff0":
                        that.player1 = new Player((i+0.5)*tileW, (j+0.5)*tileH, _stage, 1, _fx, 0, _audio);
                        that.player1.setPosition(that.player1.x, that.player1.y, this.width*tileW, this.height*tileH);
                        that.player1.level = that;
                        break;
                    case "0ffff":
                        that.player2 = new Player((i+0.5)*tileW, (j+0.5)*tileH, _stage, 2, _fx, map1.canvas.width, _audio);
                        that.player2.setPosition(that.player2.x, that.player2.y, this.width*tileW, this.height*tileH);
                        that.player2.level = that;
                        break;
                    case "ff0ff":
                        that.end = new End((i+0.5)*tileW, (j+0.5)*tileH, _stage);
                        break;
                    case "ffaaaa":
                        pickups.push({x: (i+0.35)*tileW, y: (j+0.25)*tileH, type: "tutu"});
                        break;
                    case "0ff0":
                        pickups.push({x: (i+0.35)*tileW, y: (j+0.25)*tileH, type: "metal"});
                        break;
                    case "0aaff":
                        pickups.push({x: (i+0.35)*tileW, y: (j+0.25)*tileH, type: "pomp"});
                        break;
                    case "808080":
                        pickups.push({x: (i+0.35)*tileW, y: (j+0.25)*tileH, type: "normal"});
                        break;
                    case 'ffffff':
                        break;
                    default:
                        console.log("Unknown color: ", hexa, "at", j, i);
                }
            }
        }
        // initialisation images de la map 
        imagesMap = [];
        for (var j=0; j < currentMap.length; j++) {
            imagesMap[j] = [];
            for (var i=0; i < currentMap[j].length; i++) {
                switch (currentMap[j][i]) {
                    case 0: // rien sur la case 
                        switch (Math.floor(Math.random() * 20)) {
                            case 1: imagesMap[j][i] = road2; break;       
                            case 2: imagesMap[j][i] = road3; break;       
                            case 3: imagesMap[j][i] = road4; break;       
                            case 4: imagesMap[j][i] = road5; break;       
                            case 5: imagesMap[j][i] = road6; break;       
                            case 6: imagesMap[j][i] = road7; break;       
                            case 7: imagesMap[j][i] = road8; break;       
                            case 8: imagesMap[j][i] = road9; break;
                            default: imagesMap[j][i] = road;
                        }
                        break;
                    case 1: // case de bordure
                        if (currentMap[j-1][i] == 1 && currentMap[j+1][i] == 1) {
                            switch (Math.floor(Math.random() * 20)) {
                                case 1: imagesMap[j][i] = wall_borderless_2; break;       
                                case 2: imagesMap[j][i] = wall_borderless_3; break;       
                                case 3: imagesMap[j][i] = wall_borderless_4; break;       
                                case 4: imagesMap[j][i] = wall_borderless_5; break;       
                                case 5: imagesMap[j][i] = wall_borderless_6; break;       
                                case 6: imagesMap[j][i] = wall_borderless_7; break;       
                                case 7: imagesMap[j][i] = wall_borderless_8; break;       
                                case 8: imagesMap[j][i] = wall_borderless_9; break;
                                default: imagesMap[j][i] = wall_borderless;
                            }
                        }
                        else if (currentMap[j-1][i] == 1 && currentMap[j+1][i] == 0) {
                            imagesMap[j][i] = wall_borderbottom;    
                        }
                        else if (currentMap[j-1][i] == 0 && currentMap[j+1][i] == 1) {
                            imagesMap[j][i] = wall_bordertop;    
                        }
                        else {
                            imagesMap[j][i] = wall_bordered;
                        }
                }
            }
        }
        
        // initialisation des pickups
        for (var k = 0; k < pickups.length; ++k) {
            that.pickups.push(new Pickup(pickups[k].x, pickups[k].y, _stage, _fx, pickups[k].type, that.player1));
            that.pickups.push(new Pickup(pickups[k].x, pickups[k].y, _stage, _fx, pickups[k].type, that.player2));
        }
        that.drawMap(); // TODO improve
        if (callback) callback();
        // debugger;
    };


    /**
     *  Mise à jour de la liste d'ennemis.
     */
    this.update = function(tick) {

        if (!this.loaded || !this.player1 || !this.player2) return;

        for (var i=0; i < this.enemies.length; i++) {
            var e = this.enemies[i];
            e.update(tick);

            var coords = this.player1.getCoordsOnViewport(e.x, e.y, e.width, e.height);
            if (coords) {
                e.showAt(1, coords.x, coords.y);
            }
            else {
                e.hide(1);
            }

            coords = this.player2.getCoordsOnViewport(e.x, e.y, e.width, e.height);
            if (coords) {
                e.showAt(2, coords.x, coords.y);
            }
            else {
                e.hide(2);
            }
        }

        // zone de fin
        this.end.update(tick);        
        coords = this.player1.getCoordsOnViewport(this.end.x, this.end.y);
        if (coords) {
            this.end.show(1, coords.x, coords.y);
        }
        else {
            this.end.hide(1);
        }
        coords = this.player2.getCoordsOnViewport(this.end.x, this.end.y);
        if (coords) {
            this.end.show(2, coords.x, coords.y);
        }
        else {
            this.end.hide(2);
        }

        // detection de la fin du jeu
        if (this.player1.distanceTo(this.end.x, this.end.y) < tileW/2 && this.player2.distanceTo(this.end.x, this.end.y) < tileW / 2) {
            this.player1.movingDirection.x = this.player1.movingDirection.y = 0;
            this.player2.movingDirection.x = this.player2.movingDirection.y = 0;
            _game.victory();
        }
    }


    /**
     *  Dessin de la map
     */
    this.drawMap = function(tick) {
        
        if (!currentMap) return false;
        if (!this.player1) return false;
        if (!this.player2) return false;
        if (!this.loaded) return false;
        
        this.drawMapForPlayer(this.player1, ctxMap1);
        this.drawMapForPlayer(this.player2, ctxMap2);

        return true;
    }

    /**
     *  Dessin de la map pour le joueur en paramètre
     */
    this.drawMapForPlayer = function (player, ctx) {

        var player1_tile_x = Math.floor(player.x / tileW);
        var player1_tile_y = Math.floor(player.y / tileH);

        start_x = player1_tile_x - Math.floor(map1.canvas.width / 2 / tileW) - 1;
        end_x = player1_tile_x + Math.floor(map1.canvas.width / 2 / tileW) + 2;
        start_y = player1_tile_y - Math.floor(map1.canvas.height / 2 / tileH) - 1;
        end_y = player1_tile_y + Math.floor(map1.canvas.height / 2 / tileH) + 2;
        
        if (start_x < 0) {
            start_x = 0;   
        }
        if (end_x >= currentMap[0].length) {
            end_x = currentMap[0].length-1;   
        }
        if (start_y < 0) {
            start_y = 0;   
        }
        if (start_y >= currentMap.length) {
            start_y = currentMap.length - 1;  
        }

        for (var i = start_x; i < end_x; ++i) {
            for (var j = start_y; j < end_y; ++j) {
                var image = imagesMap[j][i];                
                var x = (map1.canvas.width / 2) - ((player.x - ((i) * tileW)));
                var y = (map1.canvas.height / 2) - ((player.y - ((j) * tileH)));
                if (!image) {
                    console.log("erreur", j, i, currentMap[j][i]);
                    console.log(image.src);
                }
                ctx.drawImage(image, 0, 0, image.width, image.height, x, y, tileW, tileH);
            }
        }
    }
    

    /**
     *  Remontée du Game Over
     */
    this.gameOver = function() {
        _game.gameOver();
    }


    /**
     *  Teste sur les coordonnées en paramètre sont sur une tuile correcte.
     */
    this.isWalkingOnSunshine = function(x, y) {
        // TODO v2 --> /!\ différentes tuiles walkable
        var i = Math.floor(x / tileW);
        var j = Math.floor(y / tileH);
        return currentMap[j][i] == 0 || currentMap[j][i] >= 10;
    }

    this.getWorldWidth = function() {
        if (!currentMap) return;
        return tileW * currentMap[0].length;
    }

    this.getWorldHeight = function() {
        if (!currentMap) return;
        return tileH * currentMap.length;
    }

    this.setPlayers = function (_player1, _player2) {

        this.player1 = _player1;
        _player1.level = this;
        this.player2 = _player2;
        _player2.level = this;

    }    
}


Level.maps = [
    { id: "tuto", name: "Tutorial", desc: "Tutorial map.<br>Easy to begin...", author: "Raphael" },  
    { id: "2", name: "Raphael 2", desc: "Second map.", author: "Raphael" },
    { id: "2ouf", name: "Map de ouf", desc: "Une map de ouf. C'est ouffissime !", author: "Raphael" },
    { id: "test", name: "Map de test", desc: "Test map, two knights, one exit.", author: "Fred" },
    { id: "1", name: "Demonstration map", desc: "Can you beat this?", author: "Marius, Fred" },
];


function End(_x, _y, _stage) {

    this.x = _x;
    this.y = _y;

    this.particules = [];

    var cont1 = new createjs.Container();
    var cont2 = new createjs.Container();
    
    var g = new createjs.Graphics();
    g.beginFill("#FFFFFF").drawEllipse(0,0,50,5);
    
    var s1 = new createjs.Shape(g);
    cont1.addChild(s1);
    
    var s2 = new createjs.Shape(g);
    cont2.addChild(s2);
    
    for (var i=0; i < 40; i++) {
        var sh1 = new createjs.Shape();
        // particules rondes
        var t = Math.random()*2+2 | 0;
        sh1.graphics.beginFill("#FFFFFF").drawCircle(0,0,t);
        // object caching
        //sh1.cache(-t,-t,t*2,t*2);
        sh1.x = Math.random() * 40 + 5;
        sh1.y = 0;
        sh1.decY = (Math.random()*10 | 0 + 5) / 30;
        sh1.decAlpha = (Math.random() * 10 + 1) / 1000;
        sh1.maxY = -30;
        sh1.alpha = 1;
        sh1.visible = true;
        
        this.particules.push(sh1);
        
        if (i % 2 == 0) {
            cont1.addChild(sh1);
        }
        else {
            cont2.addChild(sh1);
        }
    }

    cont1.visible = false;
    cont2.visible = false;

    _stage.addChild(cont1);
    _stage.addChild(cont2);

    this.show = function(which, cx, cy) {
        if (which == 1) {
            cont1.x = cx - 25;
            cont1.y = cy + 20;
            cont1.visible = true;
        }
        else {
            cont2.x = cx - 25;
            cont2.y = cy + 20;
            cont2.visible = true;
        }
    }
    this.hide = function(which) {
        if (which == 1) {
            cont1.visible = false;
        }
        else {
            cont2.visible = false;
        }
    }

    this.update = function(tick) {
        for (var i=0; i < this.particules.length; i++) {
            var sh1 = this.particules[i];
            if (sh1.y < sh1.maxY || sh1.alpha <= 0) {
                sh1.y = 0;
                sh1.alpha = 1;
                sh1.x = Math.random() * 40 | 0 + 5;
                sh1.decY = (Math.random()*10 | 0 + 5) / 20;
                sh1.decAlpha = (Math.random() * 10 + 1) / 1000;
            }
            else {
                sh1.y -= sh1.decY;
                sh1.alpha -= sh1.decAlpha;
            }
        }
    }
}
