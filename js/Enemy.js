function Enemy(_x, _y, _kind, _stage, _level) {

    // 0: idle, 1: running after player
    var state = 0;
    var chased = null;

    this.audio = _level.audio;

    // coordinates
    this.x = _x;
    this.y = _y;

    // movement
    this.movingDirection = {x: 0, y: 0};
    this.speed = 0.075;

    // distance that triggers player following
    var epsilon = 130;
    var distanceGameOver = 10;

    // speed of the enemy
    var speed = 0.06;

    var imgSS = "";
    switch (_kind) {
        case "tutu":
            imgSS = "./img/sprite_gob_tutu.png";
            break;
        case "pomp":
            imgSS = "./img/sprite_skeleton.png";
            break;
        case "metal":
            imgSS = "./img/sprite_zombie.png";
            break;
    }

    this.size = { width: 19, height: 28 };

    var kind = _kind;
    this.getKind = function() {
        return kind;
    }

    this.level = _level;

    var data = {
        images: [ imgSS ],
        frames: { width: this.size.width, height: this.size.height },
        animations: {
                stand_face: [8, 8],
                stand_back: [9, 9],
                stand_left: [10, 10],
                stand_right: [11, 11],
                run_face: [0, 1, "run_face", 0.05],
                run_back: [2, 3, "run_back", 0.05],
                run_left: [4, 5, "run_left", 0.05],
                run_right: [6, 7, "run_right", 0.05]
            }
    };
    var spriteSheet = new createjs.SpriteSheet(data);
    var animation1 = new createjs.Sprite(spriteSheet, "stand_face");
    var animation2 = new createjs.Sprite(spriteSheet, "stand_face");

    animation1.regX = animation2.regX = 6;
    animation1.regY = animation2.regY = 20;
    animation1.scaleX = animation2.scaleX = 1;
    animation1.scaleY = animation2.scaleY = 1;

    _stage.addChild(animation1);
    _stage.addChild(animation2);

    this.hide = function(which) {
        if (which == 1) {
            animation1.visible = false;
        }
        else {
            animation2.visible = false;
        }
    }
    this.hide(1);
    this.hide(2);

    this.showAt = function(which, cx, cy) {
        if (which == 1) {
            animation1.x = cx;
            animation1.y = cy;
            animation1.visible = true;
        }
        else {
            animation2.x = cx;
            animation2.y = cy;
            animation2.visible = true;
        }
    }

    /**
     *  Distance to player in parameter
     */
    this.distanceTo = function(player) {
        return Math.sqrt((this.x-player.x)*(this.x-player.x)+(this.y-player.y)*(this.y-player.y));
    }

    this.update = function (tick) {
        var player1 = _level.player1;
        var player2 = _level.player2;
       switch (state) {
           case 0:  // en attente
               if (this.distanceTo(player1) < epsilon && kind != player1.kind) {
                   state = 1;
                   chased = player1;
                    this.audio.play(kind + "_" + (Math.floor(Math.random() * 2) + 1))
               }
               else if (this.distanceTo(player2) < epsilon && kind != player2.kind) {
                   state = 1;
                   chased = player2;
                   this.audio.play(kind + "_" + (Math.floor(Math.random() * 2) + 1))
               }
               break;
           case 1:  // en poursuite du personnage
               if (this.distanceTo(chased) > epsilon || kind == chased.kind) {
                   state = 0;
                   this.movingDirection.x = 0;
                   this.movingDirection.y = 0;
               }
               break;
       }
        if (state == 1) {
            // recompute movement vector
           if (this.y > chased.y + 5) {
                this.movingDirection.y = -1;
                this.movingDir = "back";
           }
           else if (this.y < chased.y - 5) {
                this.movingDirection.y = 1;
                this.movingDir = "face";
           }
           else {
                this.movingDirection.y = 0;
           }
            if (this.x > chased.x + 5) {
                this.movingDirection.x = -1;
                this.movingDir = "left";
           }
           else if (this.x < chased.x - 5) {
                this.movingDirection.x = 1;
                this.movingDir = "right";
           }
           else {
                this.movingDirection.x = 0;
           }

        }
        this.deplacement(tick);
    }

    this.movingDir = "face";

   this.deplacement = function(tick) {
        // ATTENTION point de référence (x, y) == centre du personnage
        var newX = this.x + this.movingDirection.x * tick.delta * this.speed;
        var newY = this.y + this.movingDirection.y * tick.delta * this.speed;

        if (this.level && this.level.isWalkingOnSunshine(newX, newY)) {
            this.x = newX;
            this.y = newY;
        }

       if (this.y > _level.player1.y) {
            _stage.setChildIndex(animation1, _stage.getNumChildren()-1);
       }
        else {
            _stage.setChildIndex(animation1, 0);
     }

       if (this.y > _level.player2.y) {
            _stage.setChildIndex(animation2, _stage.getNumChildren()-1);
       }
        else {
            _stage.setChildIndex(animation2, 0);
     }
        // determine si le sprite est affiché à l'écran
        this.updateAnimation();

       if (kind != _level.player1.kind && this.distanceTo(_level.player1) < distanceGameOver) {
            _level.gameOver();
       }
       else if (kind != _level.player2.kind && this.distanceTo(_level.player2) < distanceGameOver) {
            _level.gameOver();
       }
    }


    this.isMoving = function() {
        return this.movingDirection.x != 0 || this.movingDirection.y != 0;
    }


    this.lastAnim = "";
    this.updateAnimation = function () {
        var anim = "";
        if (this.isMoving()) {
            anim = "run_";
        }
        else {
            anim = "stand_";
        }
        anim += this.movingDir;
        if (anim != this.lastAnim) {
            animation1.gotoAndPlay(anim);
            animation2.gotoAndPlay(anim);
            this.lastAnim = anim;
        }
    }


}