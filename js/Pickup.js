function Pickup (x, y, stage, fx, kind, player) {
  this.world_x = x;
  this.world_y = y;

  this.stage = stage;
  this.fx = fx;

 this.kind = kind;
    this.taken = false;
  this.bitmap = null;

  this.init();

  this.player = player;

};

Pickup.prototype = {
  init: function () {
      var data = {
        images: [ "./img/sprites_potions.png" ],
        frames: { width: 19, height: 25 },
        animations: {
            tutu: [0, 0],
            pomp: [1, 1],
            metal: [2, 2],
            normal: [3, 3]
        }
    };
    var shape = new createjs.Sprite(new createjs.SpriteSheet(data),this.kind);
    shape.x = this.x;
    shape.y = this.y;
    shape.scaleX = shape.scaleY = 1;
    this.bitmap = shape;

    this.stage.addChild(this.bitmap);
  },

  update: function () {
    if (this.taken) return;
    if (!this.player) {
      console.log("pickup player null");
      return;
    }
    var player = this.player;
    var coord = player.getCoordsOnViewport(this.world_x, this.world_y, 15, 25);
    if (coord) {
        this.bitmap.x = coord.x;
        this.bitmap.y = coord.y;
        this.bitmap.visible = true;
        if (coord.y > player.y) {
            this.stage.setChildIndex(this.bitmap, this.stage.getNumChildren()-1);
        }
        else {
            this.stage.setChildIndex(this.bitmap, 0);
        }
        if (player.canTake(this.world_x, this.world_y)) {
            player.takes(this);
            this.taken = true;
            this.bitmap.visible = false;
            // TODO : respawn somewhere else?
        }
    }
    else {
        this.bitmap.visible = false;
    }
  }
}
