function Player(_x, _y, _stage, id, _fx, offset_x, audio_engine) {

    this.audio = audio_engine;

    this.x = _x;
    this.y = _y;

    this.offsetX = offset_x;

    this.stage = _stage;

    this.world = { width: 0, height: 0 };
    this.size = { width: 19, height: 29 };
    this.scale = 1;

    this.movingDirection = {x: 0, y: 0};
    this.speed = 0.08;

    this.setPosition = function (x, y, worldW, worldH) {
        this.x = x;
        this.y = y;
        this.world.width = worldW;
        this.world.height = worldH;
        this.movingDirection.x = 0;
        this.movingDirection.y = 0;
    }

    this.setPositionOnCanvas = function () {
        currentAnimation.y = this.stage.canvas.height / 2;
    }

    this.getBounds = function() {
        return (id == 1) ?
               { top: this.y - _stage.canvas.height / 2,
                 left: this.x - _stage.canvas.width / 4,
                 right: this.x + _stage.canvas.width / 4,
                 bottom: this.y + _stage.canvas.height / 2
               } :
                { top: this.y - _stage.canvas.height / 2,
                 left: this.x - _stage.canvas.width / 4,
                 right: this.x + _stage.canvas.width / 4,
                 bottom: this.y + _stage.canvas.height / 2
               }
            ;
    }

    this.getCoordsOnViewport = function(x, y, w, h) {
        if (!h) h = 0;
        if (!w) w = 0;
        var bounds = this.getBounds();
        if (y < bounds.top || y + h > bounds.bottom || x < bounds.left || x + w > bounds.right) {
            return null;
        }
        return { x: x - bounds.left + this.offsetX, y: y - bounds.top };
    }


    var that = this;

    /** Spritesheet generator */
    function genererSpriteSheet(url, defaut) {
        var data = {
        images: [ url ],
        frames: { width: that.size.width, height: that.size.height },
        animations: (id == 1) ?
            {   stand_face: [8, 8],
                stand_back: [9, 9],
                stand_left: [10, 10],
                stand_right: [11, 11],
                run_face: [0, 1, "run_face", 0.1],
                run_back: [2, 3, "run_back", 0.1],
                run_left: [4, 5, "run_left", 0.1],
                run_right: [6, 7, "run_right", 0.1]
            } :
            {   stand_face: [20, 20],
                stand_back: [21, 21],
                stand_left: [22, 22],
                stand_right: [23, 23],
                run_face: [12, 13, "run_face", 0.1],
                run_back: [14, 15, "run_back", 0.1],
                run_left: [16, 17, "run_left", 0.1],
                run_right: [18, 19, "run_right", 0.1]
            }
        };
        var spriteSheet = new createjs.SpriteSheet(data);
        var anim = new createjs.Sprite(spriteSheet, defaut);
        anim.regX = 6;
        anim.regY = 20;
        anim.x = (id == 1) ? _stage.canvas.width / 4 : _stage.canvas.width * 0.75;
        anim.y = _stage.canvas.height / 2;
        anim.scaleX = anim.scaleY = that.scale;
        anim.visible = false;
        that.stage.addChild(anim);
        return anim;
    }

    var animNormal = genererSpriteSheet("./img/sprites_knights.png", "stand_face");
    var animTutu = genererSpriteSheet("./img/sprite_knights_tutu.png", "stand_face");
    var animPompadour = genererSpriteSheet("./img/sprite_knights_pompadour.png", "stand_face");
    var animMetal = genererSpriteSheet("./img/sprite_knights_metal.png", "stand_face");

    var currentAnimation = animNormal;
    currentAnimation.visible = true;


    this.move = function(which, boo) {
        switch (which) {
            case 'up':
                if (this.movingDirection.y != 1 || boo != false) {
                    this.movingDirection.y = (boo) ? -1 : 0;
                }
                break;
            case 'down':
                if (this.movingDirection.y != -1 || boo != false) {
                    this.movingDirection.y = (boo) ? 1 : 0;
                }
                break;
            case 'left':
                if (this.movingDirection.x != 1 || boo != false) {
                    this.movingDirection.x = (boo) ? -1 : 0;
                }
                break;
            case 'right':
                if (this.movingDirection.x != -1 || boo != false) {
                    this.movingDirection.x = (boo) ? 1 : 0;
                }
                break;
        }

        if (this.movingDirection.x > 0) {
            this.movingDir = "right";
        }
        else if (this.movingDirection.x < 0) {
            this.movingDir = "left";
        }
        else if (this.movingDirection.y < 0) {
            this.movingDir = "back";
        }
        else if (this.movingDirection.y > 0) {
            this.movingDir = "face";
        }

    }

    this.isMoving = function() {
        return this.movingDirection.x != 0 || this.movingDirection.y != 0;
    }

    this.update = function (tick) {
        // ATTENTION point de référence (x, y) == centre du personnage

        var newX = this.x + this.movingDirection.x * tick.delta * this.speed;
        if (newX < this.size.width*this.scale / 2) {
            newX = this.size.width*this.scale / 2;
        }
        else if (newX + this.size.width*this.scale / 2 > this.world.width) {
            newX = this.world.width - this.size.width*this.scale / 2;
        }

        var newY = this.y + this.movingDirection.y * tick.delta * this.speed;
        if (newY < this.size.height*this.scale / 2) {
            newY = this.size.height*this.scale / 2;
        }
        else if (newY + this.size.height*this.scale / 2 > this.world.height) {
            newY = this.world.height - this.size.height*this.scale / 2;
        }

        var newX2 = (this.movingDirection.x > 0) ? newX + this.size.width/2 : newX;
        var newY2 = (this.movingDirection.y > 0) ? newY + this.size.height/4 : newY;

        if (this.level && this.level.isWalkingOnSunshine(newX, newY) && this.level.isWalkingOnSunshine(newX2, newY2)) {
            this.x = newX;
            this.y = newY;
        }

        this.updateAnimation();

        // update particles
        if (this.particles) {
            this.particles.update(tick);
        }

        this.updateSpells();
    }


    this.lastAnim = "";
    this.updateAnimation = function () {
        var anim = "";
        if (this.isMoving()) {
            anim = "run_";
        }
        else {
            anim = "stand_";
        }
        anim += this.movingDir;
        if (anim != this.lastAnim) {
            currentAnimation.gotoAndPlay(anim);
            this.lastAnim = anim;
        }
    }

    // utility
    this.distanceTo = function(x, y) {
        return Math.sqrt((this.x-x)*(this.x-x)+(this.y-y)*(this.y-y));
    }

    // potions
    this.canTake = function(coords_x, coords_y) {
        return ((this.x - coords_x)*(this.x - coords_x) + (this.y-this.size.height/4 - coords_y)*(this.y-this.size.height/4 - coords_y)) < 128;
    }

    this.potions = [];
    this.takes = function(pickup) {
        this.audio.play('pick_' + pickup.kind)
        this.potions.push({
            type: pickup.kind,
            pickupObject: pickup,
            shape: null
        });
        if (this.potions.length > 5) {
            this.potions.shift();
        }
    }
    this.useSpell = function () {
        if (this.potions.length) {
            spell = this.potions.shift();
            if (spell.shape) {
                this.stage.removeChild(spell.shape);
            }
            return spell.type;
        }
    }
    this.updateSpells = function () {

        for (var i = 0; i < this.potions.length; ++i) {
            if (!this.potions[i].shape) {
                this.potions[i].shape = this.potions[i].pickupObject.bitmap.clone();
                this.potions[i].shape.y = currentAnimation.y - 35;
                this.potions[i].shape.visible = true;
                this.stage.addChild(this.potions[i].shape)
                // debugger;
            }
            if (i == 0) {
                this.potions[i].shape.x = currentAnimation.x + 10;
            } else {
                this.potions[i].shape.x = currentAnimation.x - 25 - (15 * i);
            }
        }
    }


    // essai avec les particules
    this.particles = new Particles(_fx, this);
    this.receiveSpell = function(kind) {
        this.audio.play("send_skin_" + kind);
        var that = this;
        this.particles.applyTo(id, kind, function () {that.audio.play('swap_skin_1')});
    }
    this.readyToReceiveSpell = function () {
        return this.particles.ready();
    }


    this.kind = "normal";
    this.setKind = function(name) {
        this.kind = name;
        // console.log(name);
        switch (name) {

            case "metal":
                animMetal.x = currentAnimation.x;
                animMetal.y = currentAnimation.y;
                animMetal.gotoAndPlay(this.lastAnim);
                currentAnimation.visible = false;
                currentAnimation = animMetal;
                break;

            case "normal":
                animNormal.x = currentAnimation.x;
                animNormal.y = currentAnimation.y;
                animNormal.gotoAndPlay(this.lastAnim);
                currentAnimation.visible = false;
                currentAnimation = animNormal;
                break;
            case "tutu":
                animTutu.x = currentAnimation.x;
                animTutu.y = currentAnimation.y;
                animTutu.gotoAndPlay(this.lastAnim);
                currentAnimation.visible = false;
                currentAnimation = animTutu;
                break;
            case "pomp":
                animPompadour.x = currentAnimation.x;
                animPompadour.y = currentAnimation.y;
                animPompadour.gotoAndPlay(this.lastAnim);
                currentAnimation.visible = false;
                currentAnimation = animPompadour;
                break;
        }
        currentAnimation.visible = true;
    }

}