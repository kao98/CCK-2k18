function Particles(_fx, _player) {

    this.fx = _fx;
    this.kind = "normal";

    this.generate = function (size, col) {
        var expl = [];
        for (var i=0; i < size; i++) {
            var sh1 = new createjs.Shape();
            //var col = "hsl(" + h + ", " + s + "%, " + l + "%)";
            // particules rondes
            var t = Math.random()*4+4 | 0;
            sh1.graphics.beginFill(col).drawCircle(0,0,t);
            // object caching
            //sh1.cache(-t,-t,t*2,t*2);
            sh1.visible = false;
            this.fx.addChild(sh1);
            expl.push(sh1);
        }
        expl.visible = 0;
        return expl;
    }

    this.audioCallback = null;

    var particles = this.generate(40, "hsl(330,100%,71%)");
    var trajectoire = this.generate(1, "hsl(330,100%,71%)");

    this.ready = function () {
        return particles.visible <= 0 && trajectoire.visible <= 0;
    }

    this.reset = function(kind) {

        this.kind = kind;
        var col = "";
        // console.log(kind);
        switch (kind) {
            case 'tutu':
            col = "hsl(330,100%,71%)";
            break;
            case 'pomp':
            col = "hsl(227,100%,76%)";
            break;
            case 'metal':
            col = "hsl(0,0%,0%)";
            break;
            default:
            col = "hsl(0,100%,50%)";
        }
        // console.log(col);
        for (var i = 0; i < particles.length; ++i) {
            this.fx.removeChild(particles[i])
        }
        for (var i = 0; i < trajectoire.length; ++i) {
            this.fx.removeChild(trajectoire[i])
        }

        particles = this.generate(40, col);
        trajectoire = this.generate(1, col);
        return;

    }

    this.applyTo = function(player, kind, audioCallback) {
        if (particles.visible > 0 || trajectoire.visible > 0) return;

        this.reset(kind);
        this.audioCallback = audioCallback;

        var startX = (player == 2) ? _fx.canvas.width / 4 : _fx.canvas.width * 0.75;
        var targetX = (player == 1) ? _fx.canvas.width / 4 : _fx.canvas.width * 0.75;
        var startY = _fx.canvas.height / 2;
        for (var i=0; i < trajectoire.length; i++) {
            var sh = trajectoire[i];
            sh.x = startX;
            sh.y = startY;
            sh.startY = startY;
            sh.targetX = targetX;
            sh.demi = _fx.canvas.width / 2;
            sh.ampl = Math.random()*20+ 10;
            sh.dY = Math.random()*3 | 0 + 1;
            sh.dX = Math.random()*1 | 0 + 10;
            sh.visible = true;
            sh.decAlpha = 0.005 * (Math.random() * 0.5 + 1 | 0);
        }
        trajectoire.visible = trajectoire.length;
        trajectoire.to = player;
    }


    this.pschitt = function(player) {
        if (particles.visible > 0)
            return false;

        if (this.audioCallback) {
            this.audioCallback();
        }
        var x = (player == 1) ? _fx.canvas.width / 4 : _fx.canvas.width * 0.75;
        var y = _fx.canvas.height / 2;
        for (var i=0; i < particles.length; i++) {
            var sh1 = particles[i];
            sh1.x = x + (Math.random() * 20 | 0) - 5;
            sh1.y = y + (Math.random() * 20 | 0);
            sh1.alpha = 1;
            sh1.visible = true;
            sh1.decAlpha = 0.005 * (Math.random() * 0.5 + 1 | 0);
            // sh1.decX = (Math.random()-0.5) * (Math.random()*20);
            sh1.decY = (Math.random() * 0.4);
        }
        particles.visible = particles.length;
        setTimeout(function() { _player.setKind(this.kind); }.bind(this), 1000); // TODO improve
        return true;
    }


    this.update = function(tick) {
        if (trajectoire.visible > 0) {
            for (var i=0; i < trajectoire.length; i++) {
                if (trajectoire[i].visible) {
                    trajectoire[i].x += (trajectoire[i].x < trajectoire[i].targetX) ? trajectoire[i].dX : -trajectoire[i].dX;
                    var ratio = Math.abs(trajectoire[i].x - trajectoire[i].targetX) / (_fx.canvas.width/2);
                    trajectoire[i].y = trajectoire[i].startY - trajectoire[i].ampl * Math.sin(Math.PI * ratio)
                    if (Math.abs(trajectoire[i].x - trajectoire[i].targetX) < 10) {
                        trajectoire[i].visible = false;
                        trajectoire.visible--;
                        if (particles.visible == 0) {
                            this.pschitt(trajectoire.to);
                        }
                    }
                }
            }
        }
        if (particles.visible > 0) {
            for (var i=0; i < particles.length; i++) {
                if (particles[i].visible) {
                    particles[i].y -= particles[i].decY;
                    particles[i].alpha -= (particles[i].decAlpha * (tick.delta / (1000/60)));
                    if (particles[i].alpha <= 0) {
                        particles[i].visible = false;
                        particles.visible--;
                    }
                }
            }
        }
    }

}