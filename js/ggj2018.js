window.onload = function() {

    var _ggjGame;

    /** Canvas identification and stage */
    var canvas = document.getElementById("gameCanvas");
    var mapCanvas1 = document.getElementById("mapCanvas1");
    var mapCanvas2 = document.getElementById("mapCanvas2");
    var fogCanvas = document.getElementById("fogCanvas");
    var fxCanvas = document.getElementById('fxCanvas');

    var viewWidth = 800;
    var viewHeight = viewWidth * (window.innerHeight / window.innerWidth);

    canvas.width = fogCanvas.width = fxCanvas.width = viewWidth;
    canvas.height = mapCanvas1.height = mapCanvas2.height = fogCanvas.height = fxCanvas.height = viewHeight;

    mapCanvas1.width = mapCanvas2.width = viewWidth / 2;

    var stage = new createjs.Stage(canvas);
    var map1 = new createjs.Stage(mapCanvas1);
    var map2 = new createjs.Stage(mapCanvas2);
    var fog = new createjs.Stage(fogCanvas);
    var fx = new createjs.Stage(fxCanvas);    
    
    /** Game initialization */    
    _ggjGame = new Game(stage, map1, map2, fog, fx, new Audio());

    
    var fileToLoads = [
        "./maps/map1.png",
        "./maps/maptuto.png",
        "./maps/map2.png",
        "./maps/maptest.png",
        "./maps/map2ouf.png",
        "./img/champ_borderless_2.png",
        "./img/champ_borderless_3.png",
        "./img/champ_borderless_4.png",
        "./img/champ_borderless_5.png",
        "./img/champ_borderless_6.png",
        "./img/champ_borderless_7.png",
        "./img/champ_borderless_8.png",
        "./img/champ_borderless_9.png",
        "./img/champ.png",
        "./img/champ_borderless.png",
        "./img/champ_borderless_bas.png",
        "./img/champ_borderless_haut.png",
        "./img/chevalier_pompadour.png",
        "./img/chevalier_tutu.png",
        "./img/fond_parchemin.png",
        "./img/metalgob.png",
        "./img/sprite_gob_tutu.png",
        "./img/sprite_knights_pompadour.png",
        "./img/sprite_knights_metal.png",
        "./img/sprite_knights_tutu.png",
        "./img/sprite_skeleton.png",
        "./img/sprite_zombie.png",
        "./img/sprites_knights.png",
        "./img/sprites_potions.png",
        "./img/terre.png",
        "./img/terre_2.png",
        "./img/terre_3.png",
        "./img/terre_4.png",
        "./img/terre_5.png",
        "./img/terre_6.png",
        "./img/terre_7.png",
        "./img/terre_8.png",
        "./img/terre_9.png"
    ];

    var loader = new createjs.LoadQueue(false);
    loader.on("progress", function(e) {
        document.getElementById("progress-bar").innerText = "Loading: " + (Math.round(e.progress * 100)) + "%";
    });
    loader.on("complete", function(e) {
        document.getElementById("progress-bar").style.display = 'none';
        document.getElementById("btnStart").style.display = 'block';    
        _ggjGame.loadedAssets(this);
    });
    loader.loadManifest(fileToLoads);

    /** Ticker management */
    createjs.Ticker.addEventListener("tick", function (event) {
        // update game
        _ggjGame.tick(event);
    });
    createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
    createjs.Ticker.setFPS(60);
    

    var menuThemeInstance = null;
    //createjs.Sound.play('menuTheme');

    
    /****************************************** 
     ****       GESTION DES COMMANDES       ***
     ******************************************/

    /**
     * Classe pour les commandes
     */
    function Command(t, b, l, r, b1, b2, b3) {
        this.left = l;
        this.up = t;
        this.right = r;
        this.down = b;
        this.button1 = b1;
        this.button2 = b2;
        this.button3 = b3;
    }

    // commandes de base (par défaut)
    var command1 = new Command(90, 83, 81, 68, 65, 69, 9);
        // up-arr down-arr left-arr right-arr ù ` (mac) enter
    var command2 = new Command(38, 40, 37, 39, 165, 192, 13);
        // z s q d a e tab 
    
    // association des commandes aux deux joueurs
    var playerOneCommands = command1;
    var playerTwoCommands = command2;
    
    // gestion du game pad
    if ("getGamepads" in navigator) {
        var prompt = "To begin using your gamepad, connect it and press any button!";
        console.log(prompt);
        window.addEventListener('gamepadconnected', function(_e) {
            console.log("connection event");
        });
        window.addEventListener('gamepaddisconnected', function(_e) {
            console.log("disconnection event");
        });
    }
    
    /**
     *  Mapping des commandes du joueur au clavier
     */
    document.addEventListener("keydown", function(e) {
        //allow for WASD and arrow control scheme
        //cross browser issues exist
        if (!e) {
            var e = window.event;
        }
        e.preventDefault();
        // console.log(e.keyCode);
        switch (e.keyCode) {
            case playerOneCommands.up: // up
                _ggjGame.movePlayer1('up', true);
                break;
            case playerOneCommands.down: // down
                _ggjGame.movePlayer1('down', true);
                break;
            case playerOneCommands.left: // left
                _ggjGame.movePlayer1('left', true);
                break;
            case playerOneCommands.right: // right
                _ggjGame.movePlayer1('right', true);
                break;
            case playerTwoCommands.up: // up
                _ggjGame.movePlayer2('up', true);
                break;
            case playerTwoCommands.down: // down
                _ggjGame.movePlayer2('down', true);
                break;
            case playerTwoCommands.left: // left
                _ggjGame.movePlayer2('left', true);
                break;
            case playerTwoCommands.right: // right
                _ggjGame.movePlayer2('right', true);
                break;
            case playerOneCommands.button3: // button 3 (launch spell)
                _ggjGame.launchSpell(1);
                break;
            case playerTwoCommands.button3: // button 3 (launch spell)
                _ggjGame.launchSpell(2);
                break;
            case 27:    // escape
                _ggjGame.pause();
                break;
        }
        return false;
    });

    document.addEventListener("keyup", function(e) {
        //allow for WASD and arrow control scheme
        //cross browser issues exist
        if (!e) {
            var e = window.event;
        }
        e.preventDefault();
        // console.log(e.keyCode);
        switch (e.keyCode) {
            case playerOneCommands.up: // up
                _ggjGame.movePlayer1('up', false);
                break;
            case playerOneCommands.down: // down
                _ggjGame.movePlayer1('down', false);
                break;
            case playerOneCommands.left: // left
                _ggjGame.movePlayer1('left', false);
                break;
            case playerOneCommands.right: // right
                _ggjGame.movePlayer1('right', false);
                break;
            case playerTwoCommands.up: // up
                _ggjGame.movePlayer2('up', false);
                break;
            case playerTwoCommands.down: // down
                _ggjGame.movePlayer2('down', false);
                break;
            case playerTwoCommands.left: // left
                _ggjGame.movePlayer2('left', false);
                break;
            case playerTwoCommands.right: // right
                _ggjGame.movePlayer2('right', false);
                break;
        }
        return false;
    });
    
    /** 
     *  Ecouteurs pour le menu 
     */
    document.getElementById("btnStart").addEventListener("click", function(e) {
        _ggjGame.toMapMenu();
    });
    
    document.getElementById("btnInfo").addEventListener("click", function(e) {
        document.getElementById("infos").style.display = "block";
        document.getElementById("header").style.display = "none";
    });

    document.getElementById("infos").addEventListener("click", function(e) {
        document.getElementById("infos").style.display = "none";
        document.getElementById("header").style.display = "block";
    });
    
    document.querySelector("#bcPause .btnRetry").addEventListener("click", _ggjGame.restart.bind(_ggjGame));
    document.querySelector("#bcGameover .btnRetry").addEventListener("click", _ggjGame.restart.bind(_ggjGame));
    document.querySelector("#bcPause .btnQuit").addEventListener("click", _ggjGame.toMainMenu.bind(_ggjGame));
    document.querySelector("#bcGameover .btnQuit").addEventListener("click", _ggjGame.toMainMenu.bind(_ggjGame));
    document.querySelector("#bcMap .btnQuit").addEventListener("click", _ggjGame.exitMapMenu.bind(_ggjGame));
    
}