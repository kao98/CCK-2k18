/**
 *  Class Game
 */
function Game(_stage, _map1, _map2, _fog, _fx, _audio) {

    /** Pointer to stages */
    var stage = _stage;
    var map1 = _map1;
    var map2 = _map2;
    var fog = _fog;
    var fx = _fx;
    var loaded = false;

    this.audio_engine = _audio;

    stage.snapToPixelEnabled = true;
    map1.snapToPixelEnabled = true;
    map2.snapToPixelEnabled = true;
    fog.snapToPixelEnabled = true;

    /* global labels */
    var fontSize = stage.canvas.height / 25 | 0; //18;
    var fpsLabel = new createjs.Text("-- fps", "bold " + fontSize + "px Arial", "#FF0");
	fpsLabel.x = 10;
	fpsLabel.y = 20;

    /* game controls */
    var started = false;
    var paused = false;
    var gameover = false;
    var debug = false;

    /* creation du niveau */
    var level = new Level(map1, map2, this, stage, _audio, _fx);

    var isMovingWithGamepad = false;
    
    var mapDrawn = false;

    /** initialisation de l'affichage (barre de chargement) **/
    document.getElementById("btnStart").style.display = "none";
    document.getElementById("progress-bar").innerText = "Loading...";
    document.getElementById("progress-bar").style.display = "block";
    
    // ajout du label pour les FPS
    fx.addChild(fpsLabel);
    fpsLabel.visible = debug;
    
    
    this.debug = function() {
        debug = !debug;
        fpsLabel.visible = debug;
    }

    this.loadedAssets = function(loader) {
        level.loadedAssets(loader);    
    }
    
    
    this.loadLevel = function(id) {
        level.load(id, function () {
            this.audio_engine.startMainTheme();
            window.document.getElementById("main").style.display = "block";
            window.document.getElementById("header").style.display = "none";
            loaded = true;
        }.bind(this));
    }
    
    
    this.gameOver = function() {
        // alert("Game over");
        this.audio_engine.pauseMainTheme(true);
        this.audio_engine.play('hit');
        started = false;
        var that = this;
        window.setTimeout(function () {
            that.audio_engine.play('game_over');
            document.getElementById("bcGameover").style.display = "block";
        }, 200);
    }

    this.victory = function() {
        // alert("victory");
        this.audio_engine.pauseMainTheme(true);
        started = false;
        var that = this;
        that.audio_engine.play('Victory');
        document.getElementById("bcGameover").style.display = "block";
    }

    
    /**
     *  Démarre le jeu.
     */
    this.start = function (lvl) {
        if (!started) {
            stage.removeAllChildren();
            fx.removeAllChildren();
            document.getElementById("header").style.display = "none";
            document.getElementById("infos").style.display = "none";
            document.getElementById("bcGameover").style.display = "none";
            document.getElementById("bcPause").style.display = "none";
            document.getElementById("bcMap").style.display = "none";
            document.getElementById("main").style.display = "block";
            this.loadLevel(lvl);
            started = true;
            paused = false;
            gameover = false;        
        }
    };

    
    /** 
     *  Met le jeu en pause ou enlève la pause.
     */
    this.pause = function () {
        if (started) {
            paused = !paused;
            this.audio_engine.pauseMainTheme(paused);
            document.getElementById("bcPause").style.display = (paused) ? "block" : "none";
        }
    };


    /** 
     *  Aller au menu principal (page d'accueil)
     */
    this.toMainMenu = function() {
        this.audio_engine.pauseMainTheme(true);
        this.audio_engine.startMenuTheme();
        document.getElementById("header").style.display = "block";
        document.getElementById("infos").style.display = "none";
        document.getElementById("bcGameover").style.display = "none";
        document.getElementById("bcPause").style.display = "none";
        document.getElementById("bcMap").style.display = "none";
        document.getElementById("main").style.display = "none";
        paused = false;
        started = false;
    }
    
    /**
     *  Quitter le menu des maps
     */
    this.exitMapMenu = function() {
        document.getElementById("header").style.display = "block";
        document.getElementById("bcMap").style.display = "none";
    }
    
    
    this.restart = function () {
        started = false;
        this.start(level.getCurrentLevel());   
    }


    this.toMapMenu = function() {
        if (! this.audio_engine.themeInstance) {
            this.audio_engine.startMenuTheme();   
        }
        // clear map list
        this.initMapList(document.getElementById("mapList"));        
    
        // show map list
        document.getElementById("header").style.display = "none";
        document.getElementById("infos").style.display = "none";
        document.getElementById("bcGameover").style.display = "none";
        document.getElementById("bcPause").style.display = "none";
        document.getElementById("main").style.display = "none";
        document.getElementById("bcMap").style.display = "block";
        paused = false;
        started = false;
    }
    
    
    

    /** Handle tick */
    this.tick = function(event) {

        //return;
        if (!started || paused || !loaded) return;

        this.update(event);

        stage.update();
        //  fog.update();       // removed because fog is not moving
        fx.update();
        level.update(event);

        if (!mapDrawn || level.player1.isMoving() || level.player2.isMoving() || this.resizeCanvas()) {
            mapDrawn = level.drawMap();
            this.addFogOfWar();
        }


        fpsLabel.text = Math.round(createjs.Ticker.getMeasuredFPS()) + " fps";
    };

    
    this.addFogOfWar = function () {
        let ctx2 = fog.canvas.getContext('2d');
        let ctx3 = stage.canvas.getContext('2d');
        let pX = fog.canvas.width / 4;
        let pX2 = fog.canvas.width * (3/4);
        let pY = fog.canvas.height / 2;
        let r1 = 100;
        let r2 = 160;
        let density = .5;
        let hideFill = 'rgba( 0, 0, 0, .8 )';

        ctx2.globalCompositeOperation = 'source-over';
        ctx2.clearRect( 0, 0, fog.canvas.width, fog.canvas.height );
        ctx2.fillStyle = hideFill;
        ctx2.fillRect ( 0, 0, fog.canvas.width, fog.canvas.height );

        var radGrd = ctx2.createRadialGradient( pX, pY, r1, pX, pY, r2 );
        radGrd.addColorStop(  0, 'rgba( 0, 0, 0,  1 )' );
        radGrd.addColorStop( .8, 'rgba( 0, 0, 0, .1 )' );
        radGrd.addColorStop(  1, 'rgba( 0, 0, 0,  0 )' );

        var radGrd2 = ctx2.createRadialGradient( pX2, pY, r1, pX2, pY, r2 );
        radGrd2.addColorStop(  0, 'rgba( 0, 0, 0,  1 )' );
        radGrd2.addColorStop( .8, 'rgba( 0, 0, 0, .1 )' );
        radGrd2.addColorStop(  1, 'rgba( 0, 0, 0,  0 )' );

        ctx2.globalCompositeOperation = 'destination-out';
        ctx2.fillStyle = radGrd;
        ctx2.fillRect( pX - r2, pY - r2, r2*2, r2*2 );

        ctx2.fillStyle = radGrd2;
        ctx2.fillRect( pX2 - r2, pY - r2, r2*2, r2*2 );

        // hide characters except where we can see.  Can this be done in ctx2?
        /*
        ctx3.clearRect( 0, 0, fog.canvas.width, fog.canvas.height );

        // draw "characters"
        //TODO

        // hide characters except for in current location
        ctx3.globalCompositeOperation = 'destination-in';
        ctx3.fillStyle = radGrd;
        ctx3.fillRect( 0, 0, fog.canvas.width, fog.canvas.height );
        */
    };

    this.update = function (tick) {

        if (level.player1 && level.player2) {
            this.checkGamepad();
            level.player1.update(tick);
            level.player2.update(tick);
        }

        for (var i = 0; i < level.pickups.length; ++i) {
            level.pickups[i].update();
        }
    };

    
    this.resizeCanvas = function () {

        var viewWidth = 800;
        var viewHeight = viewWidth * (window.innerHeight / window.innerWidth);

        if (Math.abs(viewHeight - stage.canvas.height) > 1) {
            stage.canvas.height = map1.canvas.height = map2.canvas.height = fog.canvas.height = fx.canvas.height = viewHeight;
            level.player1.setPositionOnCanvas();
            level.player2.setPositionOnCanvas();
            return true;
        } 
        
        return false;
    };

    

    // deplacement des personnages
    this.movePlayer1 = function(which, boo) {
        level.player1.move(which, boo);
    }
    this.movePlayer2 = function(which, boo) {
        level.player2.move(which, boo);
    }

    // launch a spell
    this.launchSpell = function(id) {
        // console.log("ls ", id);
        var spell = false;
        if (id == 1) {
            if (level.player2.readyToReceiveSpell() && (spell = level.player1.useSpell())) {
                level.player2.receiveSpell(spell);
            }
        }
        else {
            if (level.player1.readyToReceiveSpell() && (spell = level.player2.useSpell())) {
                level.player1.receiveSpell(spell);
            }
        }
    }


    this.checkGamepad = function() {
        var gp = navigator.getGamepads()[0];
        if (!gp) return;

        var axeLF = gp.axes[1] || gp.axes[3];
        var axeUD = gp.axes[0] || gp.axes[2];

        if (axeLF < -0.5) {
            this.movePlayer1('right', false);
            isMovingWithGamepad = 'left';
            this.movePlayer1('left', true);
        } else if(axeLF > 0.5) {
            this.movePlayer1('left', false);
            isMovingWithGamepad = 'right';
            this.movePlayer1('right', true);
        } else if(axeUD < -0.5) {
            this.movePlayer1('down', false);
            isMovingWithGamepad = 'up';
            this.movePlayer1('up', true);
        } else if(axeUD > 0.5) {
            this.movePlayer1('up', false);
            isMovingWithGamepad = 'down';
            this.movePlayer1('down', true);
        } else {
            this.movePlayer1('up', false);
            this.movePlayer1('down', false);
            this.movePlayer1('left', false);
            this.movePlayer1('right', false);
            isMovingWithGamepad = false;
        }
    }
    
    
    this.initMapList = function(mapList) {        
        while (mapList.hasChildNodes()) {
            mapList.removeChild(mapList.firstChild);   
        }
        // populate maps
        var that = this;
        for (var i=0; i < Level.maps.length; i++) {
            var img = document.createElement("img");
            img.src = "./maps/map" + Level.maps[i].id + ".png";
            var p = document.createElement("p");
            p.innerHTML = "<strong>" + Level.maps[i].name + "</strong><br>" + Level.maps[i].desc + "<br><br>By " + Level.maps[i].author;
            var div = document.createElement("div");
            div.appendChild(img);
            div.appendChild(p);
            div.id = Level.maps[i].id;
            div.addEventListener("click", function(e) {
                that.start(this.id); 
            });
            mapList.appendChild(div);
        }
    }

}