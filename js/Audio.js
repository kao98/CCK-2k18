var Audio = function () {

	var that = this;
	var assetPath = "./sounds/";
    var sounds = [
		{src: "CCK_Music_01.ogg", id: "theme2"},
		{src: "CCK_07_menu.mp3", id: "theme1"},
		{src: "Swap_skin_01.mp3", id: "swap_skin_1"},
		{src: "Send_skin/Send_skin_01.mp3", id: "send_skin_tutu"},
		{src: "Send_skin/Send_skin_02.mp3", id: "send_skin_pomp"},
		{src: "Send_skin/Send_skin_03.mp3", id: "send_skin_metal"},
		{src: "Send_skin/Send_skin_01.mp3", id: "send_skin_normal"},
		{src: "Gobelin/Gobelin_cry_01.mp3", id: "tutu_1"},
		{src: "Gobelin/Gobelin_cry_02.mp3", id: "tutu_2"},
		{src: "Metalzombie_01.mp3", id: "metal_1"},
		{src: "Metalzombie_02.mp3", id: "metal_2"},
		{src: "Bonesman_cry_01.mp3", id: "pomp_1"},
		{src: "Bonesman_cry_02.mp3", id: "pomp_2"},
		{src: "Pick_up_01.mp3", id: "pick_tutu"},
		{src: "Pick_Up_02.mp3", id: "pick_pomp"},
		{src: "Pick_up_03.mp3", id: "pick_metal"},
		{src: "Pick_up_01.mp3", id: "pick_normal"},
		{src: "Game_Over.mp3", id: "game_over"},
		{src: "Victory.mp3", id: "Victory"},
		{src: "Mort_cri.mp3", id: "hit"}
    ];
    createjs.Sound.alternateExtensions = ["ogg"];    // if the passed extension is not supported, try this extension
    //createjs.Sound.on("fileload", function (e) {that.handleLoad(e) }); // call handleLoad when each sound loads
    //createjs.Sound.registerSounds(sounds, assetPath);

    this.audioQueue = new createjs.LoadQueue();
    this.audioQueue.installPlugin(createjs.Sound);
    this.audioQueue.on("fileload", function (e) { that.handleLoad.call(that, e) }); // call handleLoad when each sound loads
    this.audioQueue.loadManifest(sounds, true, assetPath);

	this.themeInstance = null;
	this.mainThemeStarted = false;
};

Audio.prototype = {

	handleLoad: function (e) {
		if (e.item.id === "theme1" && !this.mainThemeStarted) {
			this.startMenuTheme();
		}
	},

	menuEnterHandler: function () {
        createjs.Sound.play('menuHover', new createjs.PlayPropsConfig().set({
	        interrupt: createjs.Sound.INTERRUPT_ANY,
	        loop: 0,
	        volume: 0.8
	    }));
    },

    menuLeaveHandler: function () {
        createjs.Sound.play('menuOut', new createjs.PlayPropsConfig().set({
	        interrupt: createjs.Sound.INTERRUPT_ANY,
	        loop: 0,
	        volume: 0.8
	    }));
    },

    play: function (id, loop) {
		// console.log("audio playing ", id);
    	createjs.Sound.play(id, new createjs.PlayPropsConfig().set({
		    interrupt: createjs.Sound.INTERRUPT_ANY,
		    loop: loop || 0,
		    volume: 0.6
		}));
    },

	startMenuTheme: function (callback) {
    	var that = this;

    	var ppc = new createjs.PlayPropsConfig().set({
            interrupt: createjs.Sound.INTERRUPT_ANY,
            loop: -1,
            volume: 0
        });


    	this.themeInstance = createjs.Sound.play('theme1', ppc);

        createjs.Tween.get(this.themeInstance)
            .to({volume: 1}, 1000);

        if (callback) callback();

    },

    startMainTheme: function (callback) {
		var that = this;
		this.mainThemeStarted = true;

		if (this.themeInstance) {
			this.themeInstance.stop();
			this.themeInstance = null;
		}

    	var ppc = new createjs.PlayPropsConfig().set({
            interrupt: createjs.Sound.INTERRUPT_ANY,
            loop: -1,
            volume: 0.4
        });

    	this.themeInstance = createjs.Sound.play('theme2', ppc);

        if (callback) callback();

    },

    restart: function (callback) {
    	if (this.themeInstance) {
    		this.themeInstance.stop();
    		this.themeInstance = null;
    	}

    	this.startMainTheme(null);

    	this.play('start');
    },

    pauseMainTheme: function (pause) {
		this.themeInstance.paused = pause;
    },

    mute: function (mute) {
    	createjs.Sound.volume = mute ? 0 : 1;
    },

    boom: function () {
		createjs.Sound.play('boom', new createjs.PlayPropsConfig().set({
		    interrupt: createjs.Sound.INTERRUPT_ANY,
		    loop: 0,
		    volume: 0.8
		}));
		this.pauseMainTheme(true);
    }

};